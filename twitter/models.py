from django.db import models


class User(models.Model):
	username = models.CharField('Username', max_length=50)
	email = models.EmailField()
	password = models.CharField('Password', max_length=50)
